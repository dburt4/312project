﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TSP
{
    class State
    {
        public double[,] StateMatrix { get; set; }

        public int Bound { get; set; }

        public int CurrCity { get; set; }

        public List<int> VisitedCities { get; set; } 

        public State(double[,] arr, int bound, List<int> visitedCities)
        {
            this.StateMatrix = arr;
            this.Bound = bound;
            this.CurrCity = 0;
            this.VisitedCities = visitedCities;
        }

        //Copy constructor:
        public State(State old)
        {
            this.StateMatrix = old.StateMatrix;
            this.Bound = old.Bound;
            this.CurrCity = old.CurrCity;
            this.VisitedCities = new List<int>();
            for(int i=0; i<old.VisitedCities.Count;i++)
            {
                this.VisitedCities.Add(old.VisitedCities[i]);
            }
        }
    }
}
