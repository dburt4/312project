using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Resources;
using System.Text;
using System.Drawing;
using System.Timers;

namespace TSP
{
    class ProblemAndSolver
    {
        private class TSPSolution
        {
            /// <summary>
            /// we use the representation [cityB,cityA,cityC] 
            /// to mean that cityB is the first city in the solution, cityA is the second, cityC is the third 
            /// and the edge from cityC to cityB is the final edge in the path.  
            /// you are, of course, free to use a different representation if it would be more convenient or efficient 
            /// for your node data structure and search algorithm. 
            /// </summary>
            public ArrayList 
                Route;

            public TSPSolution(ArrayList iroute)
            {
                Route = new ArrayList(iroute);
            }


            /// <summary>
            ///  compute the cost of the current route.  does not check that the route is complete, btw.
            /// assumes that the route passes from the last city back to the first city. 
            /// </summary>
            /// <returns></returns>
            public double costOfRoute()
            {
                // go through each edge in the route and add up the cost. 
                int x;
                City here; 
                double cost = 0D;
                
                for (x = 0; x < Route.Count-1; x++)
                {
                    here = Route[x] as City;
                    cost += here.costToGetTo(Route[x + 1] as City);
                }
                // go from the last city to the first. 
                here = Route[Route.Count - 1] as City;
                cost += here.costToGetTo(Route[0] as City);
                return cost; 
            }
        }

        #region private members
        private const int DEFAULT_SIZE = 25;
        
        private const int CITY_ICON_SIZE = 5;

        private bool TEST = false;
        /// <summary>
        /// the cities in the current problem.
        /// </summary>
        private City[] Cities;
        /// <summary>
        /// a route through the current problem, useful as a temporary variable. 
        /// </summary>
        private ArrayList Route;
        /// <summary>
        /// best solution so far. 
        /// </summary>
        private TSPSolution bssf; 

        /// <summary>
        /// how to color various things. 
        /// </summary>
        private Brush cityBrushStartStyle;
        private Brush cityBrushStyle;
        private Pen routePenStyle;


        /// <summary>
        /// keep track of the seed value so that the same sequence of problems can be 
        /// regenerated next time the generator is run. 
        /// </summary>
        private int _seed;
        /// <summary>
        /// number of cities to include in a problem. 
        /// </summary>
        private int _size;

        /// <summary>
        /// random number generator. 
        /// </summary>
        private Random rnd;
        #endregion

        #region public members.
        public int Size
        {
            get { return _size; }
        }

        public int Seed
        {
            get { return _seed; }
        }
        #endregion

        public const int DEFAULT_SEED = -1;

        #region Constructors
        public ProblemAndSolver()
        {
            initialize(DEFAULT_SEED, DEFAULT_SIZE);
        }

        public ProblemAndSolver(int seed)
        {
            initialize(seed, DEFAULT_SIZE);
        }

        public ProblemAndSolver(int seed, int size)
        {
            initialize(seed, size);
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// reset the problem instance. 
        /// </summary>
        private void resetData()
        {
            Cities = new City[_size];
            Route = new ArrayList(_size);
            bssf = null; 

            for (int i = 0; i < _size; i++)
                Cities[i] = new City(rnd.NextDouble(), rnd.NextDouble());

            cityBrushStyle = new SolidBrush(Color.Black);
            cityBrushStartStyle = new SolidBrush(Color.Red);
            routePenStyle = new Pen(Color.LightGray,1);
            routePenStyle.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
        }

        private void initialize(int seed, int size)
        {
            this._seed = seed;
            this._size = size;
            if (seed != DEFAULT_SEED)
                this.rnd = new Random(seed);
            else
                this.rnd = new Random();
            this.resetData();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// make a new problem with the given size.
        /// </summary>
        /// <param name="size">number of cities</param>
        public void GenerateProblem(int size)
        {
            this._size = size;
            resetData(); 
        }

        /// <summary>
        /// return a copy of the cities in this problem. 
        /// </summary>
        /// <returns>array of cities</returns>
        public City[] GetCities()
        {
            City[] retCities = new City[Cities.Length];
            Array.Copy(Cities, retCities, Cities.Length);
            return retCities;
        }

        /// <summary>
        /// draw the cities in the problem.  if the bssf member is defined, then
        /// draw that too. 
        /// </summary>
        /// <param name="g">where to draw the stuff</param>
        public void Draw(Graphics g)
        {
            float width  = g.VisibleClipBounds.Width-45F;
            float height = g.VisibleClipBounds.Height-15F;
            Font labelFont = new Font("Arial", 10);

            g.DrawString("n(c) means this node is the nth node in the current solution and incurs cost c to travel to the next node.", labelFont, cityBrushStartStyle, new PointF(0F, 0F)); 

            // Draw lines
            if (bssf != null)
            {
                // make a list of points. 
                Point[] ps = new Point[bssf.Route.Count];
                int index = 0;
                foreach (City c in bssf.Route)
                {
                    if (index < bssf.Route.Count -1)
                        g.DrawString(" " + index +"("+c.costToGetTo(bssf.Route[index+1]as City)+")", labelFont, cityBrushStartStyle, new PointF((float)c.X * width + 3F, (float)c.Y * height));
                    else 
                        g.DrawString(" " + index +"("+c.costToGetTo(bssf.Route[0]as City)+")", labelFont, cityBrushStartStyle, new PointF((float)c.X * width + 3F, (float)c.Y * height));
                    ps[index++] = new Point((int)(c.X * width) + CITY_ICON_SIZE / 2, (int)(c.Y * height) + CITY_ICON_SIZE / 2);
                }

                if (ps.Length > 0)
                {
                    g.DrawLines(routePenStyle, ps);
                    g.FillEllipse(cityBrushStartStyle, (float)Cities[0].X * width - 1, (float)Cities[0].Y * height - 1, CITY_ICON_SIZE + 2, CITY_ICON_SIZE + 2);
                }

                // draw the last line. 
                g.DrawLine(routePenStyle, ps[0], ps[ps.Length - 1]);
            }

            // Draw city dots
            foreach (City c in Cities)
            {
                g.FillEllipse(cityBrushStyle, (float)c.X * width, (float)c.Y * height, CITY_ICON_SIZE, CITY_ICON_SIZE);
            }

        }

        /// <summary>
        ///  return the cost of the best solution so far. 
        /// </summary>
        /// <returns></returns>
        public double costOfBssf ()
        {
            if (bssf != null)
                return (bssf.costOfRoute());
            else
                return -1D; 
        }

        /// <summary>
        ///  solve the problem.  This is the entry point for the solver when the run button is clicked
        /// right now it just picks a simple solution. 
        /// </summary>
        public void solveProblem()
        {
            bool timeUp = false;
            int max = 0;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            //int x;
            //Route = new ArrayList(); 
            //// this is the trivial solution. 
            //for (x = 0; x < Cities.Length; x++)
            //{
            //    Route.Add( Cities[Cities.Length - x -1]);
            //}
            //// call this the best solution so far.  bssf is the route that will be drawn by the Draw method. 
            //bssf = new TSPSolution(Route);
            
            double[,] startState = GenerateStartState();
            
            //Get an initial bound and simple, starting route to work with. Set that to the starting BSSF:
            double[,] copyStartState = new double[_size,_size];
            copyStartState = copy(startState);
            int initialBound = GetBound(copyStartState);
            copyStartState = copy(startState);
            Route = QuickSolve(copyStartState);
            bssf = new TSPSolution(Route);
            //System.Diagnostics.Debug.WriteLine(ToString());

            //Now implement the stack and find the children of the state you are in:
            Stack<State> agenda = new Stack<State>();

            //Push the first one on there:
            agenda.Push(new State(startState, initialBound ,new List<int>()));

            while (agenda.Count > 0 && !timeUp) //Need a couple other checks here as well
            {
                State currentState = agenda.Pop();
                Print("Just popped state:");
                Print(currentState);

                if (currentState.Bound <= bssf.costOfRoute())
                {
                    //Gets the children for this state and sorts them with the best last:
                    List<State> children = FindSucessors(currentState);
                    sortChildren(children);
                    //Print("SORTED : -----------------------");
                    //foreach(State child in children)
                    //{
                    //    Print(child);
                    //}

                    //Add those to the stack ... if their bound is less than the BSSF so far and greater than the lower bound:
                    foreach(State child in children)
                    {
                        TimeSpan ts = stopwatch.Elapsed;
                        //System.Diagnostics.Debug.WriteLine("Elapsed " + ts.Seconds + " and " + ts.Minutes);
                        if (ts.Minutes > 0) timeUp = true;
                        if (child.Bound <= bssf.costOfRoute()) //TODO: make sure these are in fact the numbers that we want to compare and add a TIME check  
                        {
                            
                            if (isCriterion(child))
                            {
                                //System.Diagnostics.Debug.WriteLine("UPDATED BSSF from " + bssf.costOfRoute());
                                bssf = new TSPSolution(GetRoute(child));
                                //System.Diagnostics.Debug.WriteLine("to: " + bssf.costOfRoute());
                            }

                            else
                            {
                                Print("Pushed: ");
                                Print(child);
                                agenda.Push(child);
                                int check = agenda.Count;
                                if (check > max) max = check;
                            }
                            
                        }
                    }       
                }

                
                
                
            }
           
            stopwatch.Stop();
            Console.WriteLine(ToString());
            Console.WriteLine(max);
            TimeSpan span = stopwatch.Elapsed;
            Program.MainForm.tbElapsedTime.Text = span.Minutes + " : " + span.Seconds +" : " + span.Milliseconds; 

            // update the cost of the tour. 
            Program.MainForm.tbCostOfTour.Text = " " + bssf.costOfRoute();
            Program.MainForm.tbCostOfTour.Text = " " + bssf.costOfRoute();
            // do a refresh. 
            Program.MainForm.Invalidate();

        }

        //Returns the route based on the given state. Route is an array list of Cities
        //@Pre the state is totally solved 
        public ArrayList GetRoute(State state)
        {
            ArrayList toReturn = new ArrayList();
            double[,] matrix = state.StateMatrix;
            int rowIndex = 0;

            for (int i = 0; i < _size; i++)
            {
                for (int j = 0; j < _size; j++)
                {
                    //If it finds the one in the row that isn't a -1 
                    if (matrix[rowIndex, j] != -1)
                    {
                        //Add it:
                        toReturn.Add(Cities[j]);
                        rowIndex = j;
                        break;
                    }
                }
            }

            return toReturn;
        }

        /**
         * Will return true if every state has been visited in this state. Otherwise, false
         */
        public bool isCriterion(State state)
        {
            //I'm just going to make this one fairly dumb - ie it will just check to make sure there is only one value in each row. It won't care if there is one in each column as well.
            //That should be fine unless we are getting passed a bad state here
            double[,] matrix = state.StateMatrix;

            for (int r = 0; r < _size; r++)
            {
                bool alreadyFound = false;
                for (int c = 0; c < _size; c++)
                {
                    if (matrix[r, c] != -1)
                    {
                        //There is another non -1 number in the row:
                        if (alreadyFound)
                            return false;

                        alreadyFound = true;
                    }
                }

                //Made it through the whole row. Should have found one
                if (!alreadyFound)
                    return false;
            }
            Print("Was criterion!! :");
            Print(state);
            return true;
        }

        /**
         * A method that will find all the children for a state based on the "current city" (or row) that it is at.
         */
        public List<State> FindSucessors(State state)
        {
            List<State> toReturn = new List<State>();
            int row = state.CurrCity;
            Print("THESE ARE THE VISITED CITIES");
            foreach(int i in state.VisitedCities)
            {
                Print(i + "");
            }
            
            //Find every different combo of routes that you can take and add them to the list:
            for (int c = 0; c < _size; c++)
            {
                //List<int> alreadyVisted = new List<int>();
                
                if (state.StateMatrix[row, c] != -1 && !state.VisitedCities.Contains(c))
                {
                    //Print("There is a possible child at " + c);
                    double[,] newMatrix = copy(state.StateMatrix);
                    //Set the row to -1 except the one it's on:
                    for (int j = 0; j < _size; j++)
                    {
                        if (j != c)
                            newMatrix[row, j] = -1;
                        
                    }
                    //Sets the whole column to -1 except the one that it "took"
                    for (int j = 0; j < _size; j++)
                    {
                        if(j != row)
                            newMatrix[j, c] = -1;
                    }

                    //alreadyVisted.Add(c);

                    State newState = new State(newMatrix, GetBound(copy(newMatrix)), state.VisitedCities);
                    Print("ADDING " + row);
                    newState.VisitedCities.Add(row);
                    newState.CurrCity = c;
                    toReturn.Add(newState);
                }
            }
            return toReturn;
        }
        /*
         * Will sort the children passed to it, with the greatest bound first to the least bound so that it can be put on the stack later
         * This might kill our run time pretty bad ...
         * Currently implemented as a selection sort:
         */
        public List<State> sortChildren(List<State> children)
        {
            List<State> toReturn = new List<State>();
            for (int i = 0; i < children.Count; i++)
            {
                int highestBound = children[i].Bound;
                State highestState = children[i];
                int highestStateIndex = i;

                //Finds the next lowest one:
                for (int j = i; j < children.Count; j++)
                {
                    if (children[j].Bound > highestBound)
                    {
                        highestBound = children[j].Bound;
                        highestState = children[j];
                        highestStateIndex = j;
                    }
                }

                //Do a good old switcher-oo
                if (highestStateIndex != i)
                {
                    State temp = new State(children[i]);
                    children[i] = new State(highestState);
                    children[highestStateIndex] = new State(temp);
                    
                }
            }

            return toReturn;
        }

        /*
         * Reduces the matrix and finds the bound for it:
         */
        public int GetBound(double[,] boundState)
        {
            Print("GETTING THE BOUND");
            int bound = 0;
            Print("STATE OF MATRIX WE ARE BOUNDING:");
            Print(boundState);
            //Reduce all the rows:
            for(int r=0; r<_size;r++)
            {
                double lowestNum = 999999;
                int lowestNumIndex = -1;
                //Print("State befoe doing row operations: ");
                //Print(boundState);
                //Finds the first number in the row that isn't -1 and uses that as a starting state:
                for (int i = 0; i < _size; i++)
                {
                    if (boundState[r, i] != -1)
                    {
                        lowestNum = boundState[r, i];
                        lowestNumIndex = i;
                        break;
                    }
                }

                //find the lowest number in the row:
                for(int c=lowestNumIndex; c <_size; c++)
                {
                    if (boundState[r, c] < lowestNum && boundState[r,c] != -1) //TODO: Fix the exception here
                    {
                        lowestNum = boundState[r, c];
                        lowestNumIndex = c;
                    }
                }

                bound += (int) lowestNum;
                //Print("Lowest row num: " + lowestNum);
                //Now subtract that lowest number from everything in that row:
                for (int c = 0; c < _size; c++)
                {
                    if(boundState[r,c] != -1)
                        boundState[r, c] = boundState[r, c] - lowestNum;
                }
            }

            //--------------------------------
            
            //Now reduce all the columns that can be reduced (ie don't have a 0 in them):
            for (int c = 0; c < _size; c++)
            {
                double lowestNum = 999999;
                int lowestNumIndex = -1;

                //First a good first start state:
                for (int r = 0; r < _size; r++)
                {
                    if (boundState[r, c] != -1)
                    {
                        lowestNum = boundState[r, c];
                        lowestNumIndex = r;
                        break;
                    }
                }

                //Now find the lowest index:
                for (int r = lowestNumIndex; r < _size; r++)
                {
                    if (boundState[r, c] < lowestNum && boundState[r, c] != -1)
                    {
                        lowestNum = boundState[r, c];
                        lowestNumIndex = r;
                    }
                }

                //Subtract that from the column if there are no 0's in it:
                if (lowestNum != 0)
                {
                    for (int r = 0; r < _size; r++)
                    {
                        if (boundState[r, c] != -1)
                        {
                            boundState[r, c] = boundState[r, c] - lowestNum;
                        }    
                    }
                    bound += (int) lowestNum;
                    //Print("Lowest col num: " + lowestNum);
                }

            }

            return bound;
        }

        //Greedy algorithm to find a cheap first BSSF 
        public ArrayList QuickSolve(double[,] startState)
        {
            ArrayList toReturn = new ArrayList();

            int currentRowIndex = 0;
            int columnIndex = 0;
            Print("State before:" );
            Print(startState);
            for (int i = 0; i < _size; i++)
            {
                //Finds the lowest path in this row:
                double lowestPath = 500000;

                //A simple check that will make sure it's not starting on an index that has a value of 0
                for (int l = 1; l < _size;l++)
                {
                    if(startState[currentRowIndex,l] != 0 && startState[currentRowIndex,l] != -1)
                    {
                        columnIndex = l;
                        lowestPath = startState[currentRowIndex, l];
                        break;
                    }
                }
                    //Print("this is the current row index " + currentRowIndex);
                for (int j = 1; j < _size; j++)
                { 
                    if (startState[currentRowIndex, j] < lowestPath && startState[currentRowIndex, j] > 0)
                    {
                        lowestPath = startState[currentRowIndex, j];
                        columnIndex = j;
                        //Print("current row index was assigned to " + j);
                    }
                    
                }
                if(i== _size-1)
                {
                    columnIndex = 0;
                }
                //Print(lowestPath + "");
               // Print("current row index is now " + currentRowIndex);
                //Sets all the numbers in that column to -1 so that city wont' be visited again: ...except the optimal path one:
                for (int k = 0; k < _size; k++)
                {
                    if (k != currentRowIndex)
                    {
                       // Print("r: " + k + " c: " + columnIndex);
                        startState[k, columnIndex] = -1;

                    }
                    
                }
                currentRowIndex = columnIndex;

                //Print("After getting rid of paths: ");
                //Print(startState);

            }

            int finalRowIndex = 0;
            int finalColIndex = 0;
            //toReturn.Add(Cities[0]);

            for (int i = 0; i < _size; i++)
            {
                for (int j = 0; j < _size; j++)
                {
                    if (startState[finalRowIndex, j] != -1)
                    {
                        finalColIndex = j;
                        toReturn.Add(Cities[j]);
                        //Print("This city was added " + j);
                    }
                }
                finalRowIndex = finalColIndex;
            }
            //Console.WriteLine(toReturn.ToString());
            //Print(toReturn.Count + "");
            return toReturn;
        }

        public double[,] GenerateStartState()
        {

            double[,] toReturn = new double[Cities.Length, Cities.Length];
            for (int i = 0; i < Cities.Length; i++)
            {
                for (int j = 0; j < Cities.Length; j++)
                {
                    toReturn[i, j] = Cities[i].costToGetTo(Cities[j]);
                    if (toReturn[i, j] == 0) toReturn[i, j] = -1;
                    //Console.Write(toReturn[i,j] + " ");
                }
               // Console.WriteLine();
            }

            return toReturn;
        }

        //A convienant way to print out debug statements with an on and off feature
        public void Print(String message)
        {
            if(TEST)
                Console.WriteLine(message);
        }

        //A quick print for a state:
        public void Print(State state)
        {
            if (!TEST) return;
            Print("STATE: --------------------------------");
            Print(state.StateMatrix);
            Print("Bound: " + state.Bound);
            Print("Current city: " + state.CurrCity);
            Print("");
        }

        /**
         * Prints a state as a 2D array:
         */
        public void Print(double[,] state)
        {
            if (TEST)
            {
                for (int i = 0; i < _size; i++)
                {
                    for (int j = 0; j < _size; j++)
                    {
                        Console.Write(state[i,j] + " ");
                    }
                    Console.WriteLine();
                }
            }
        }

        /**
         * Copies a state (aka a 2D array) into a new one:
         */
        public double[,] copy(double[,] toCopy)
        {
            double[,] toReturn = new double[_size, _size];
            for (int row = 0; row < _size; row++)
            {
                for (int col = 0; col < _size; col++)
                {
                    toReturn[row, col] = toCopy[row, col];
                }
            }
            return toReturn;

        }

        public String ToString()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append('[');
            foreach (City c in bssf.Route)
            {
                builder.Append("[" + c.X + "," + c.Y + "],");
            }
            builder.Remove(builder.Length - 1, 1); //Remove extra comma at end
            builder.Append(']');

            return builder.ToString();
        }

        #endregion
    }
}
